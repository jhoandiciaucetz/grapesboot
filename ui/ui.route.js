import express from "express"
const uiRoute = express.Router()

uiRoute.get('/', (req, res)=>{
    res.render('home',{tittle:'Webpage Builder'});
});

uiRoute.all('*', (req, res) => {
    res.render('404');
});

export default uiRoute;